package com.techandsolve.reservas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "vuelo")
public class Vuelo implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_vuelo")   
    @SequenceGenerator(
        name = "id_vuelo",
        sequenceName = "id_vuelo",
        allocationSize = 1
    )
    private Integer id;
    @Column(name = "nro_vuelo", length = 6)
    private String nroVuelo;
    @OneToOne
	@JoinColumn(name = "id_origen")
    private Ciudad idOrigen;
    @OneToOne
	@JoinColumn(name = "id_destino")
    private Ciudad idDestino;
    @Column(name = "fecha")
    private String fecha;
    @Column(name = "hora_salida", length = 5)
    private String horaSalida;
    @Column(name = "hora_llegada", length = 5)
    private String horaLlegada;
    @Column(name = "sillas_disponibles")
    private Integer sillasDisponibles;
    @Column(name = "valor")
    private Integer valor;
    
    public Vuelo(){
        
    }
    
    public Vuelo(Integer id, String nroVuelo, Ciudad idOrigen, Ciudad idDestino, String fecha, 
                 String horaSalida, String horaLlegada, Integer sillasDisponibles, Integer valor){
        this.id= id;
        this.nroVuelo = nroVuelo;
        this.idOrigen = idOrigen;
        this.idDestino = idDestino;
        this.fecha = fecha;
        this.horaSalida = horaSalida;
        this.horaLlegada = horaLlegada;
        this.sillasDisponibles = sillasDisponibles;
        this.valor = valor;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNroVuelo() {
        return nroVuelo;
    }

    public void setNroVuelo(String nroVuelo) {
        this.nroVuelo = nroVuelo;
    }

    public Ciudad getIdOrigen() {
        return idOrigen;
    }

    public void setIdOrigen(Ciudad idOrigen) {
        this.idOrigen = idOrigen;
    }

    public Ciudad getIdDestino() {
        return idDestino;
    }

    public void setIdDestino(Ciudad idDestino) {
        this.idDestino = idDestino;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public Integer getSillasDisponibles() {
        return sillasDisponibles;
    }

    public void setSillasDisponibles(Integer sillasDisponibles) {
        this.sillasDisponibles = sillasDisponibles;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}
