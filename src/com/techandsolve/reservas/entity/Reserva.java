package com.techandsolve.reservas.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Entity
@Table(name = "reserva")
public class Reserva implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_reserva")   
    @SequenceGenerator(
        name = "id_reserva",
        sequenceName = "id_reserva",
        allocationSize = 1
    )
    private Integer id;
	@Column(name = "id_vuelo")
	private Integer idVuelo;
	@Column(name = "nro_identificacion")
	private String nroIdentificacion;
	@Column(name = "tipo_identificacion")
	private String tipoIdentificacion;
	@Column(name = "nombres")
	private String nombres;
	@Column(name = "apellidos")
	private String apellidos;
	
	public Reserva(){
		
	}

	public Reserva(Integer id, Integer idVuelo, String nroIdentificacion, 
				   String tipoIdentificacion, String nombres, String apellidos) {
		this.id = id;
		this.idVuelo = idVuelo;
		this.nroIdentificacion = nroIdentificacion;
		this.tipoIdentificacion = tipoIdentificacion;
		this.nombres = nombres;
		this.apellidos = apellidos;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getIdVuelo() {
		return idVuelo;
	}
	
	public void setIdVuelo(Integer idVuelo) {
		this.idVuelo = idVuelo;
	}
	
	public String getNroIdentificacion() {
		return nroIdentificacion;
	}
	
	public void setNroIdentificacion(String nroIdentificacion) {
		this.nroIdentificacion = nroIdentificacion;
	}
	
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
	public String getNombres() {
		return nombres;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	public String getApellidos() {
		return apellidos;
	}
	
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
}
