package com.techandsolve.reservas.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.techandsolve.reservas.entity.*;
import com.techandsolve.reservas.facade.AbstractFacade;

@javax.inject.Singleton
@Path("/reserva")
@Transactional
public class ReservaRestService extends AbstractFacade<Reserva> {
	
	/**
     * Referencia al administrador de entidades
     */
    @PersistenceContext(unitName = "reservas-PU")
    private EntityManager em;

    public ReservaRestService() {
            super(Reserva.class);
    }
    
    public Reserva find(Integer id) throws Exception {
        return super.find(id);
    }
    
    @GET
    @Path("/{id}")
    @Produces({ "application/json" })
    public Response getVueloById(@PathParam("id") Integer id){
    	try{
    		Reserva r = this.find(id);
    		
    		if(r != null){
    			return Response.ok(r).build();
    		}else{
    			return super.getResponseNotFound();
    		}
    	}
    	catch(Exception e){
    		return super.getResponseError(e);
    	}
    }

    @Override
    public List<Reserva> findAll()throws Exception {
        return super.findAll();
    }
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON + ";charse=tUTF-8" })
    public Response getListaVuelos(){
    	try{
    		return Response.ok(this.findAll()).build();
    	}catch(Exception e){
    		return super.getResponseError(e);
    	}
    }
    
    @POST
    @Override
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response create(Reserva entity) {
    	entity.setId(null);
    	
    	try{
    		String actualizaVuelo = this.updateVuelo(entity.getIdVuelo());
    		if(!actualizaVuelo.equals("OK")){
    			return Response.status(Response.Status.NOT_ACCEPTABLE)
    					.entity("No hay sillas disponibles para el Vuelo: " + actualizaVuelo).build();
    		}
    	}catch(Exception e){
    		return super.getResponseError(e);
    	}
    	
    	return super.create(entity);
    }
    
    private String updateVuelo(Integer id) throws Exception {
    	
    	Vuelo vuelo = em.find(Vuelo.class, id);
    	
    	if(vuelo != null){
    		if(vuelo.getSillasDisponibles().intValue() != 0){
    			vuelo.setSillasDisponibles(vuelo.getSillasDisponibles()-1);
            	em.merge(vuelo);
    		}else{
    			return vuelo.getNroVuelo();
    		}
    	}
    	
    	return "OK";
    }
    
   	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
