package com.techandsolve.reservas.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.techandsolve.reservas.entity.Vuelo;
import com.techandsolve.reservas.facade.AbstractFacade;

@javax.inject.Singleton
@Path("/vuelo")
public class VueloRestService extends AbstractFacade<Vuelo>{

	/**
     * Referencia al administrador de entidades
     */
    @PersistenceContext(unitName = "reservas-PU")
    private EntityManager em;

    public VueloRestService() {
            super(Vuelo.class);
    }
    
    @Override
    public List<Vuelo> findAll() throws Exception {
    	return super.findAll(); 
    }
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON + ";charse=tUTF-8" })
    public Response getListaVuelos(){
    	try{
    		List<Vuelo> lista = this.findAll();
    		
    		if(lista != null && !lista.isEmpty()){
    			return Response.ok(lista).build();
    		}else{
    			return super.getResponseNotFound();
    		}
    	}catch(Exception e){
    		return super.getResponseError(e);
    	}
    }
    
    public Vuelo find(Integer id) throws Exception {
        return super.find(id);
    }
    
    @GET
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON + ";charse=tUTF-8" })
    public Response getVueloById(@PathParam("id") Integer id){
    	try{
    		Vuelo v = this.find(id);
    		
    		if(v != null){
    			return Response.ok(this.find(id)).build();
    		}else{
    			return super.getResponseNotFound();
    		}
    	}
    	catch(Exception e){
    		return super.getResponseError(e);
    	}
    }
    
    @POST
    @Override
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response create(Vuelo entity) {
    	return super.create(entity);
    }

    @PUT
    @Override
    @Consumes({ MediaType.APPLICATION_JSON })
    public Response edit(Vuelo entity) {
        return super.edit(entity);
    }

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
