package com.techandsolve.reservas.facade;

import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.ws.rs.core.Response;

import com.techandsolve.reservas.entity.Vuelo;

public abstract class AbstractFacade<T> {
	
	private Class<T> entityClass;

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	protected abstract EntityManager getEntityManager();

	public Response create(T entity) {
		try{
			getEntityManager().persist(entity);
			getEntityManager().flush();
			return Response.status(Response.Status.CREATED).entity(entity.toString()).build();  
		  }catch(Exception e){
			  return getResponseError(e);
		  }
	}

	public Response edit(T entity) {
		try{
			getEntityManager().merge(entity);
			getEntityManager().flush();
			return Response.status(Response.Status.OK).entity(entity.toString()).build();  
		  }catch(Exception e){
			  return getResponseError(e);
		  }
	  }

	public void delete(Object id) throws Exception {
		Object ref = getEntityManager().getReference(entityClass, id);
		getEntityManager().remove(ref);
	}

	public T remove(T entity) throws Exception{
		getEntityManager().remove(getEntityManager().merge(entity));
		getEntityManager().flush();
		return entity;
	}

	public T find(Object id) throws Exception { 
		EntityManager em = getEntityManager();
		return em.find(entityClass, id);
	}

	public List<T> findAll() throws Exception{
		CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getEntityManager().createQuery(cq).getResultList();
	}

	public List<T> findWithNamedQuery(String namedQueryName) {
		return getEntityManager().createNamedQuery(namedQueryName).getResultList();
	}

	public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
		Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = getEntityManager().createNamedQuery(namedQueryName);
		  
		if (resultLimit > 0) {
			query.setMaxResults(resultLimit);
		}
	    
		for (Map.Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		  
		return query.getResultList();
	}

	public List<T> findByNativeQuery(String sql) {
		return getEntityManager().createNativeQuery(sql, entityClass).getResultList();
	}

	public T findSingleWithNamedQuery(String namedQueryName) {
		T result = null;
		 
		try {
			result = (T) getEntityManager().createNamedQuery(namedQueryName).getSingleResult();
		}catch (NoResultException e) {
		}
		  
		return result;
	}

	public T findSingleWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = getEntityManager().createNamedQuery(namedQueryName);
	    
		for (Map.Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		  
		T result = null;
	    
		try {
			result = (T) query.getSingleResult();
		}catch (NoResultException e) {
		}
	    
		return result;
	}
	
	public Response getResponseError(Exception e){
		return Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build();
	}
	
	public Response getResponseNotFound(){
		return Response.status(Response.Status.NO_CONTENT).build();
	}
}
